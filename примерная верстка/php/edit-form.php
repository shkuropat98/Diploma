<?php
session_start();


$conn_string = "host=localhost port=5433 dbname=diploma user=postgres password=rosmen98";
$dbconn4 = pg_connect($conn_string);

$current_collection = $_SESSION['cur_collection'];

echo $_SESSION['cur_collection'];

// данные из формы
$card_image = $_POST['picture'];
$card_title = $_POST['title'];
$card_description = $_POST['description'];
$card_link = $_POST['url'];

$card_collection = $_POST['collection-name'];

echo $card_image;
echo $card_title;
echo $card_description;
echo $card_link;
echo $card_collection;

// провести добавление данных
// добавить запись в таблицу cards
$insert_into_cards = "insert into cards(card_image, title, description, link)
values
($1, $2, $3, $4);";

$result = pg_prepare($dbconn4,"insert_into_cards", $insert_into_cards);
$result = pg_execute($dbconn4, "insert_into_cards", array($card_image, $card_title, $card_description, $card_link));
pg_free_result($result);

// взять номер последнего жлемента из cards
$get_last_card = 'select card_number 
from cards
order by card_number desc
limit 1';
$result = pg_query($dbconn4, $get_last_card);
$last_card = pg_fetch_row($result);


// добавить запись в collections
$insert_into_collections = 'update collections 
set cards_for_col = array_append(cards_for_col, $1) 
where collection_name = $2;';

$result = pg_prepare($dbconn4,"insert_into_collections", $insert_into_collections);
$result = pg_execute($dbconn4, "insert_into_collections", array($last_card[0], $card_collection));


// перенаправить на главную страницу
header ("Location: index.php?collection=$current_collection");

?>