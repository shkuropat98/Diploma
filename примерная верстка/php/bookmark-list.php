<?php
    $conn_string = "host=localhost port=5433 dbname=diploma user=postgres password=rosmen98";
    $dbconn4 = pg_connect($conn_string);
    
    if (!$dbconn4) {
        echo 'error';
    }
    else {
        
        $query = "select card_number, card_image, title, description, link
                  from cards
                  where card_number in (select unnest(cards_for_col) 
                                        from collections
                                        where collection_name = '$collection_name');";
        
        $result = pg_query($dbconn4, $query);
        
        if (!$result) {
            
        echo "Произошла ошибка.\n";
        exit;
        }
        
        while ($row = pg_fetch_row($result)) 
        {
            echo '
            <div class="bookmark-card">
                <div class="bookmark-card__img">
                    <img src="'.$row[1].'" alt="bookmark_image">
                </div>
                <div class="bookmark-card__info">
                    <div class="card-title">'.$row[2].'</div>
                    <div class="card-description">'.$row[3].'</div>
                    <div class="card-link">'.$row[4].'</div>
                </div>
            </div>
            ';
        }
    }
?>