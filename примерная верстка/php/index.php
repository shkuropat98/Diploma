<?php


    if (isset($_GET['collection'])) {
        $collection_name = $_GET['collection'];
        $_SESSION['cur_collection'] = $collection_name;         
    }


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Главная</title>
        <link rel="stylesheet" href="../css/style.css" />
        <script
            src="https://kit.fontawesome.com/2a076becfb.js"
            crossorigin="anonymous"
        ></script>
    </head>

    <body>
        <div class="wrapper">
            <div class="content">
                <div class="nav">
                    <?php include "../blocks/nav-header.php"?>
                    <div class="nav-content">
                        <div class="nav-item collections">
                            <?php include "nav-collections.php" ?>
                        </div>
                        <div class="nav-item tags">
                            <?php include "nav-tags.php" ?>
                        </div>
                    </div>
                </div>
                <div class="main">
                    <div class="main-header">
                        <div class="search-container">
                            <div class="search">
                                <form class="search-form" action="">
                                    <input
                                        placeholder="Найти"
                                        class="search-input"
                                        type="text"
                                    />
                                </form>
                            </div>
                            <button class="addButton-button">
                                <i class="fas fa-star"></i>
                                Добавить
                            </button>
                            <div class="add-popup-container">
                                <label for="add-input">URL</label>
                                <input id="add-input" type="text" />
                                <button>Сохранить</button>
                            </div>
                        </div>
                        <div class="collection-title-container">
                            <div class="collection-title">
                                <span class="icon collection"></span>
                                <span class="collection-title__text">
                                    <?php echo $collection_name?>
                                </span>
                            </div>
                            <div class="collection-additional">Сортировать</div>
                        </div>
                    </div>
                    <div class="bookmark-list">
                        <?php include("bookmark-list.php")?>
                        <!-- <div class="bookmark-card">
                        <div class="bookmark-card__img">
                            <img src="cat.png" alt="bookmark_image">
                        </div>
                        <div class="bookmark-card__info">
                            <div class="card-title">Заголовок</div>
                            <div class="card-description">Описание</div>
                            <div class="card-link">Ссылка</div>
                        </div>
                    </div>
                    <div class="bookmark-card">
                        <div class="bookmark-card__img">
                            <img src="cat.png" alt="bookmark_image">
                        </div>
                        <div class="bookmark-card__info">
                            <div class="card-title">Заголовок</div>
                            <div class="card-description">Описание</div>
                            <div class="card-link">Ссылка</div>
                        </div>
                    </div>
                    <div class="bookmark-card">
                        <div class="bookmark-card__img">
                            <img src="cat.png" alt="bookmark_image">
                        </div>
                        <div class="bookmark-card__info">
                            <div class="card-title">Заголовок</div>
                            <div class="card-description">Описание</div>
                            <div class="card-link">Ссылка</div>
                        </div>
                    </div>
                    <div class="bookmark-card">
                        <div class="bookmark-card__img">
                            <img src="cat.png" alt="bookmark_image">
                        </div>
                        <div class="bookmark-card__info">
                            <div class="card-title">Заголовок</div>
                            <div class="card-description">Описание</div>
                            <div class="card-link">Ссылка</div>
                        </div>
                    </div> -->
                    </div>
                </div>
                <div class="modal">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2>Добавить</h2>
                            <span class="close">&times;</span>
                        </div>
                        <div class="modal-body">
                            <form class="modal-form" action="edit-form.php" method="POST">
                                <div class="modal-form__content">
                                    <div class=" modal-elem modal-form__title">
                                        <label class="modal-label" for="modal-input__title">Заголовок</label>
                                        <input name="title" id="modal-input__title" type="text">                                    
                                    </div>
                                    <div class=" modal-elem modal-form__description">
                                        <label class="modal-label" for="modal-description">Описание</label>
                                        <textarea name="description" id="modal-description" cols="30" rows="3"></textarea>                                    
                                    </div>
                                    <div class=" modal-elem modal-form__collection">
                                        <label class="modal-label" for="collection">Коллекция</label>
                                        <select  name="collection-name" id="collection">
                                            <option value="collection1">col1</option>
                                            <option value="collection2">col2</option>
                                        </select>                                    
                                    </div>
                                    <!-- <div class=" modal-elem modal-form__tags">
                                        <label class="modal-label" for="modal-input__tags">Тэги</label>
                                        <input id="modal-input__tags" type="text">                                    
                                    </div> -->
                                    <div class=" modal-elem modal-form__url">
                                        <label class="modal-label" for="modal-input__url">URL</label>
                                        <input name="url" id="modal-input__url" type="text">                                    
                                    </div>
                                    <div class=" modal-elem modal-form__picture">
                                        <label class="modal-label" for="modal-form__picture">Картинка</label>
                                        <input name="picture" id="modal-form__picture" type="text">                                    
                                    </div>
                                </div>

                                <button class="addButton-button">                                    
                                    Добавить
                                </button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="../js/index.js"></script>
    </body>
</html>
