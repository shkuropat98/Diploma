"use strict"


function showModal() {
    modalWindow.classList.remove('hide-modal');
    modalWindow.classList.add('show-modal');
}
function hideModal() {
    modalWindow.classList.remove('show-modal');
    modalWindow.classList.add('hide-modal');
}

let modalWindow = document.querySelector('.modal');
let modalBackground = document.querySelector('.modal-bg');
let addButton = document.querySelector('.addButton-button');
let closeModalButton = document.querySelector('.close');

addButton.addEventListener('click', showModal);
closeModalButton.addEventListener('click', hideModal);




